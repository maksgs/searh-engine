package search.engine.mappers;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Component;
import search.engine.model.Page;
import search.engine.model.SearchResult;

/**
 * User: Maksym Goroshkevych Date: 19.06.14 Time: 10:31
 */
@Component
public class SearchResultMapper
{
	private static final String HIGHLIGHTED_PATTERN = "<em>%s</em>";

	/**
	 * maps solr query response to search result
	 * @param response
	 * 			solr query response
	 * @param query
	 * 			query, user for highlighting
	 * @return {@link SearchResult}
	 */
	public SearchResult map(QueryResponse response, String query){
		String regex = "(?i)" + query;
		SearchResult searchResult = new SearchResult();
		searchResult.setItems(response.getBeans(Page.class));
		searchResult.setTotalNum(response.getResults().getNumFound());
		//merge highlights
		for (Page page: searchResult.getItems()){
			page.setPageBody(page.getPageBody().replaceAll(regex, String.format(HIGHLIGHTED_PATTERN, query.toUpperCase())));
			page.setPageHeader(page.getPageHeader().replaceAll(regex, String.format(HIGHLIGHTED_PATTERN, query.toUpperCase())));
		}
		return searchResult;
	}
}
