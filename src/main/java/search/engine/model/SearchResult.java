package search.engine.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * User: Maksym Goroshkevych Date: 19.06.14 Time: 10:31
 */
public class SearchResult
{
	@Getter
	@Setter
	private List<Page> items;

	@Getter
	@Setter
	private long totalNum;


}
