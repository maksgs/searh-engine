package search.engine.model;

/**
 * User: Maksym Goroshkevych Date: 19.06.14 Time: 9:36
 */
public class Constants
{
	private Constants(){

	}

	public static final String ERROR_MSG = "errorMsg";
	public static final String SUCCESS_MSG = "successMsg";
	public static final String SEARCH_RESULT = "searchResult";
	public static final String QUERY = "query";
	public static final String START = "start";
	public static final String ALL_FIELDS_SOLR_QUERY = "*:*";
	public static final String HIDE_MORE_BTN = "hideMoreBtn";
	public static final String URL = "url";

}
