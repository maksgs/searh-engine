package search.engine.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;

/**
 * Represents web page data (url, html, title)
 * User: Maksym Goroshkevych Date: 18.06.14 Time: 21:57
 */
@EqualsAndHashCode
@ToString
public class Page
{
	@Getter
	@Setter
	@Field
	private String id;

	@Getter
	@Setter
	@Field
	private String url;

	@Getter
	@Setter
	@Field
	private String pageBody;

	@Getter
	@Setter
	@Field
	private String pageHeader;

	@Getter
	@Setter
	@Field
	private Date indexedDate;

	public Page(){

	}

	public Page(String url, String pageBody, String pageHeader, Date indexedDate){
		this.url = url;
		this.pageBody = pageBody;
		this.pageHeader = pageHeader;
		this.indexedDate = indexedDate;
	}

}
