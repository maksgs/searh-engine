package search.engine.services;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import search.engine.model.Constants;
import search.engine.model.Page;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * User: Maksym Goroshkevych Date: 18.06.14 Time: 21:56
 */
@Service
public class IndexationService
{
	private static final String HREF_ATTR = "href";

	@Inject
	private ConcurrentUpdateSolrServer solrServer;

	@Inject
	private HttpSolrServer httpSolrServer;

	@Value("${indexation.step}")
	private int maxStep;

	/**
	 * index all page html and sublinks with deapth maxStep
	 * @param uri - web page uri
	 */
	@Async("indexingAsyncTaskExecutor")
	public void indexPage(String uri)
	{
		//this collection will store unique urls
		System.out.println("indexation started");
		List<String> urls = new ArrayList<>();
		indexPageData(1, uri, urls);
		System.out.println("indexation finished");
	}

	private void indexPageData(int step, String uri, List<String> urls){
		if (step > maxStep){
			return;
		}
		if (urls.contains(uri)){
			return;
		}
		try{
			Document doc = Jsoup.connect(uri).get();
			//add data to solr index
			Page page = new Page(uri, doc.text(), doc.title(), new Date());
			solrServer.addBean(page);
			//todo: need to use autocommit
			solrServer.commit();
			System.out.println(String.format("indexed: %s", uri));
			//mark url as indexed
			urls.add(uri);
			Elements links = doc.select("a[href]");
			for (Element link: links){
				indexPageData(step + 1, link.attr(HREF_ATTR), urls);
			}
		}catch (IOException | SolrServerException | IllegalArgumentException e){
			//ignore as for now
		}

	}

	/**
	 * removes all data from solr
	 */
	public void clearIndex()
	{
		try
		{
			httpSolrServer.deleteByQuery(Constants.ALL_FIELDS_SOLR_QUERY);
			httpSolrServer.commit();
		}
		catch (SolrServerException | IOException e)
		{
			throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, e.getMessage());
		}
	}
}
