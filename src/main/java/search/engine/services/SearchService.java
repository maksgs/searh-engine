package search.engine.services;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;
import search.engine.mappers.SearchResultMapper;
import search.engine.model.Constants;
import search.engine.model.Page;
import search.engine.model.SearchResult;
import search.engine.utils.SpecialCharactersEscaper;

import javax.inject.Inject;

/**
 * User: Maksym Goroshkevych Date: 18.06.14 Time: 21:57
 * search for data in solr
 */
@Service
public class SearchService
{
	private static final String SOLR_SEARCH_QUERY_PATTERN = "pageBody:%s";
	private static final String SOLR_COMMON_SEARCH_QUERY_PATTERN = "%s:%s";

	@Inject
	private HttpSolrServer solrServer;

	@Inject
	private SearchResultMapper mapper;

	/**
	 * search for matched string
	 * @param query
	 * 			query
	 * @param start
	 * 			start
	 * @param size
	 * 			number of results
	 * @return {@link SearchResult}
	 * @throws SolrServerException
	 */
	public SearchResult search(String query, int start, int size) throws SolrServerException
	{
		SolrQuery solrQuery = new SolrQuery(String.format(SOLR_SEARCH_QUERY_PATTERN,
				SpecialCharactersEscaper.escape(query)));
		solrQuery.setStart(start);
		solrQuery.setRows(size);
		QueryResponse response = solrServer.query(solrQuery);
		return mapper.map(response, query);
	}

	/**
	 * checks if this page is already indexed
	 * @param query - query
	 * @return result
	 */
	public boolean existUrl(String query) throws SolrServerException
	{
		SolrQuery solrQuery = new SolrQuery(String.format(SOLR_COMMON_SEARCH_QUERY_PATTERN, Constants.URL,
				SpecialCharactersEscaper.escape(query)));
		QueryResponse response = solrServer.query(solrQuery);
		return !response.getBeans(Page.class).isEmpty();
	}

}
