package search.engine.validators;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import search.engine.services.SearchService;

import javax.inject.Inject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 19.06.14 Time: 8:35
 */
@Component
public class IndexingUrlValidator
{
	@Inject
	private MessageSource messageSource;

	@Inject
	private SearchService searchService;

	/**
	 * validates url, adds error in case if page was already indexed or in case of bad url
	 * @param url
	 * 			web page url
	 * @param locale
	 * 			user locale, user for displaying i18n messages
	 * @param errors
	 * 			list of errors
	 */
	public void validate(String url, Locale locale, List<String> errors){
		try{
			if (!validUrlAddress(url)){
				errors.add(messageSource.getMessage("url.is.not.valid.msg", null, locale));
			}
			if(searchService.existUrl(url)){
				errors.add(messageSource.getMessage("url.already.indexed.msg", null, locale));
			}
		}catch (IOException | SolrServerException ex){
			errors.add(messageSource.getMessage("url.is.not.valid.msg", null, locale));
		}
	}

	private boolean validUrlAddress(String url) throws IOException
	{
		URL u = new URL( url );
		HttpURLConnection huc =  (HttpURLConnection)  u.openConnection ();
		huc.setRequestMethod ("GET");
		huc.connect () ;
		int code = huc.getResponseCode() ;
		return code == 200;
	}
}
