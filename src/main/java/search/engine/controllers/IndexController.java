package search.engine.controllers;

import com.google.common.base.Strings;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import search.engine.model.Constants;
import search.engine.services.IndexationService;
import search.engine.validators.IndexingUrlValidator;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 18.06.14 Time: 21:48
 */
@Controller
public class IndexController
{
	private static final String INDEX_TEMPLATE = "index";

	@Inject
	private IndexationService indexationService;

	@Inject
	private IndexingUrlValidator validator;

	@Inject
	private MessageSource messageSource;

	@RequestMapping("/index")
	public String getIndexPage()
	{
		return INDEX_TEMPLATE;
	}

	/**
	 *
	 * @param uri
	 * 			web page url
	 * @param model
	 * 			page model
	 * @param locale
	 * 			user locale
	 * @return name of thymeleaf template
	 * @throws IOException
	 */
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String indexUri(@RequestParam(required = false, value = "q") String uri, Model model, Locale locale) throws IOException
	{
		List<String> errors = new ArrayList<>();
		validator.validate(uri, locale, errors);
		if (!errors.isEmpty()){
			model.addAttribute(Constants.ERROR_MSG, errors.get(0));
			return INDEX_TEMPLATE;
		}
		if (!Strings.isNullOrEmpty(uri)){
			indexationService.indexPage(uri);
			model.addAttribute(Constants.SUCCESS_MSG, messageSource.getMessage("indexing.started", null , locale));
		}

		return INDEX_TEMPLATE;
	}

	/**
	 * removes data from solr index
	 */
	@RequestMapping(value = "/index", method = RequestMethod.DELETE)
	@ResponseBody
	public void clearIndex()
	{
		indexationService.clearIndex();
	}
}
