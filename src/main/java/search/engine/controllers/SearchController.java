package search.engine.controllers;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import search.engine.model.Constants;
import search.engine.model.SearchResult;
import search.engine.services.SearchService;

import javax.inject.Inject;
import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 18.06.14 Time: 23:24
 */
@Controller
public class SearchController
{
	private static final String SEARCH_TEMPLATE = "search";
	private static final String SEARCH_RESULT_TEMPLATE = "ajax/searchResult";

	@Inject
	private SearchService searchService;

	@Inject
	private MessageSource messageSource;

	/**
	 * returns search page with first 10 results
	 * @param query
	 * 			query
	 * @param start
	 * 			result offset
	 * @param rows
	 * 			limit
	 * @param model
	 * 			page model
	 * @param locale
	 * 			user locale
	 * @return page name
	 */
	@RequestMapping("/search")
	public String getSearchPage(@RequestParam(required = false, value = "q") String query,
								@RequestParam(required = false, value = "start", defaultValue = "0") int start,
								@RequestParam(required = false, value = "rows", defaultValue = "10") int rows,
								Model model,
								Locale locale){
		query = StringEscapeUtils.unescapeXml(query);
		addSearchResult(query, model, start, rows, locale);
		model.addAttribute(Constants.QUERY, query);
		return SEARCH_TEMPLATE;
	}

	/**
	 * lear more button handler, returns search result
	 * @param query
	 * 			query
	 * @param start
	 * 			result offset
	 * @param rows
	 * 			limit
	 * @param model
	 * 			page model
	 * @param locale
	 * 			user locale
	 * @return page name
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String getSearchResultFragment(@RequestParam(required = false, value = "q") String query,
								@RequestParam(required = false, value = "start", defaultValue = "0") int start,
								@RequestParam(required = false, value = "rows", defaultValue = "10") int rows,
								Model model,
								Locale locale){
		query = StringEscapeUtils.unescapeXml(query);
		addSearchResult(query, model, start, rows, locale);
		return SEARCH_RESULT_TEMPLATE;
	}

	private void addSearchResult(String query, Model model, int start, int rows, Locale locale){
		model.addAttribute(Constants.START, start);
		if (!Strings.isNullOrEmpty(query)){
			try{
				SearchResult searchResult = searchService.search(query, start, rows);
				model.addAttribute(Constants.SEARCH_RESULT, searchResult);
				model.addAttribute(Constants.HIDE_MORE_BTN, start + 10 >= searchResult.getTotalNum());
			}catch (SolrServerException ex){
				model.addAttribute(Constants.ERROR_MSG,
						messageSource.getMessage("error.occur.msg", new Object[]{ex.getMessage()}, locale));
			}

		}
	}

}
