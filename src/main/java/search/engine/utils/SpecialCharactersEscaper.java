package search.engine.utils;

import com.google.common.base.Strings;

/**
 * User: Maksym Goroshkevych Date: 19.06.14 Time: 20:37
 */
public class SpecialCharactersEscaper
{
	private static final String SPECIAL_CHARACTERS = "(?=[]\\[+&|!(){}^\"~*?:\\\\/-])";
	private static final String SPECIAL_CHARACTERS_REPLACEMENT = "\\\\";

	private SpecialCharactersEscaper()
	{
	}

	public static String escape(String source)
	{
		if(Strings.isNullOrEmpty(source)){
			return source;
		}
		return "\"" + source.replaceAll(SPECIAL_CHARACTERS, SPECIAL_CHARACTERS_REPLACEMENT) + "\"" ;
	}
}
