/**
 * return clean html text
 * @param data - html code fragment
 */
function clean(data){
    data = data.substring(data.indexOf('<body>') + 6);
    data = data.replace('</body></html>', '');
    return data;
}

/**
 * removes all data from solr
 */
function clearIndex(){
    $.ajax({
        type: "delete",
        success : function(data) {
            showSuccessMsg('success_msg', 'Index was cleared!');
        }
    });
}

/**
 * loads more items
 */
function viewNextSearchResultItems(){
    var start = 10 + parseInt($('#searchOffset').val());
    $.ajax({
        type: "post",
        data:{
            start: start
        },
        success : function(data) {
            data = clean(data);
            //add search results
            $('#searchResultList').append(data);
            //hide more button if needed
            if ($('#hideMoreBtn').val() == 'true'){
                $('#viewMoreBtn').remove();
            }
            //remove temporary div
            $('#temporaryData').remove();
            $('#searchOffset').val(start);
        }
    });
}
