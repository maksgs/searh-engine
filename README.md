* # Quick summary #
This is search engile test project. Project has two main featues:
1. Ability to index web page html data
2. Quick search based on that data

* # Configuration #
To start application do next steps:
* Download sources.
* Add solr/sol.war file to tomcat
* Put solr/solr.xml under "/tomcat/conf/Catalina/localhost" folder, if it is not exist, create it.
* In solr.xml specify path to "solr/solr-data" folder
* Put sol/sol-libs under tomcat/libs folder.
* Run project (solr should be started at the same time). 
* Go to /${contextPath}/index or /${contextPath}/search pages

* # Used technologies #
* Java 8
* Spting MVC, Spring IOC
* Solr - search web tool based on lucene framework,
* JSoup - tool for parsing html data,
* lombok -library for generating getter, setter, equals, hashcode and toString methods using annotations
* Solrj - library for working with solr
* guava, apache-commons
* JQuery
* Thymeleaf - html "analog" of jsp
* maven
* bootstrap